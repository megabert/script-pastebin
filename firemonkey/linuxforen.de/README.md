## Allgemeines

Das Script erlaubt es einem über die Firefox-Erweiterung "Greasemonkey" 
Themen von namentlich konfigurierten Benutzern von linuxforen.de auszublenden. 
Dazu muss die Erweiterung greasemonkey installiert werden, ein neues Script angelegt 
werden und der Inhalt der Datei hide-user-threads.js in diese Datei 
eingefügt und gespeichert werden.

## Nutzer definieren

```var hide_users = ["BananenBoy","GewaltBrabbler"];```

In dieser Zeile die betreffenden Benutzernamen in Anführungszeichen und 
mit Komma getrennt eintragen und die Datei speichern.

Nach einem Reload der Seite sollten die betreffenden Beiträge ausgeblendet sein.
