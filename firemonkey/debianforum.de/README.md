## Allgemeines

Das Script erlaubt es einem über die Firefox-Erweiterung ~~Greasemonkey~~ FireMonkey
Themen von namentlich konfigurierten Benutzern von debianforum.de auszublenden. 
Dazu muss die Erweiterung greasemonkey installiert werden, ein neues Script angelegt 
werden und der Inhalt der Datei hide-user-threads.js in diese Datei 
eingefügt und gespeichert werden.

## zu versteckende Nutzer definieren

```
var hiddenUsers = ["BananenBoy","GewaltBrabbler"];
```

In dieser Zeile die betreffenden Benutzernamen in Anführungszeichen und 
mit Komma getrennt eintragen und die Datei speichern.

Nach einem Reload der Seite sollten die betreffenden Beiträge ausgeblendet sein.

## Zu versteckende Themen definieren

```
   var hiddenThreads = [
	"Voller Betreff-1 des zu versteckenden Themas",
	"Voller Betreff-2 des zu versteckenden Themas"
	];
```
	
In dieser Zeile den Betreff der zu versteckenden Themen eintragen.
