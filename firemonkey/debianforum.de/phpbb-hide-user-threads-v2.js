// ==UserScript==
// @name       DF_Hide_User_Threads_v2
// @author     some_random_dude
// @version    1
// @namespace  some_random_dude
// @match      https://debianforum.de/forum/search.php*
// @match      https://debianforum.de/forum/viewforum.php*
// @run-at     document-start
// ==/UserScript==

(() => {
  "use strict";

  // put usernames to hide here
  const hiddenUsers = [
    "username-1",
    "username-2"
  ];

  // put thread topic patterns to hide here
  const hiddenThreadPatterns = [
    "Pattern to Match for hiding #1",
    "Pattern to Match for hiding #2",
  ];

  const log_enabled = 0;

  function mylog(text) {
   if (log_enabled) {
   		console.log(text);
   }
 }

 function removeRow(row) {

    /* Since we are removing a row, shift the light and dark grey
     * backgrounds accordingly.
     */

   let prevRow = row;
   let prevClasses = row.className;
   let myRow = row.nextElementSibling;
   while (myRow) {
     const classes = myRow.className;
     myRow.className = prevClasses;
     prevClasses = classes;
     prevRow = myRow;
     myRow = prevRow.nextElementSibling;
   }
	 mylog("  removing row");
   row.remove();
 }

  function hideThreadsByThreadStarter(node) {
       const usernameElems = node.querySelectorAll?.("dt .username, dt .username-coloured") ?? [];

        for (const usernameElem of usernameElems) {
          /* There is a parallel username element with class "responsive-show"
           * in the <dt>, that shows the *last poster* on mobile. We are not
           * looking for that element here.
           */
          if (!hiddenUsers.includes(usernameElem.textContent) ||
              !usernameElem.parentElement.classList.contains("responsive-hide")) {
            continue;
          }
          mylog("found user thread of user " + usernameElem.textContent + " to hide");
          removeRow(usernameElem.closest("li.row"));
  	}
   }

 function hideThreadsByTopic(node) {
       const topicElems = node.querySelectorAll?.("div.list-inner a.topictitle") ?? [];

        for (const topicElem of topicElems) {
          // mylog("found thread with topic: " + topicElem.textContent);
          if (!hiddenThreadPatterns.some(term => new RegExp(term).test(topicElem.textContent)))  {
							// subject to hide not found -> return
            continue;
          }

          mylog("found subject to hide('" + topicElem.textContent + "')");
          removeRow(topicElem.closest("li.row"));
        }
 }

  function hideThings(mutations) {
    for (const mutation of mutations) {
      for (const node of mutation.addedNodes) {
        hideThreadsByThreadStarter(node);
        hideThreadsByTopic(node);
      }
    }  }

  const observer = new MutationObserver(hideThings);
  observer.observe(document.documentElement, { childList: true, subtree: true });
  addEventListener("load");
})();
