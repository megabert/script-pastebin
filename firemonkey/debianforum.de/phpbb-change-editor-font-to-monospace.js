/* Post Box Font Fix for DevNetwork.net phpBB Forum
 * Copyright (C) 2009 SnyDev.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 * 
 * Last update: 2022-05-07 22:00 CET
 */

// ==UserScript==
// @name             phpbb-change-editor-font-to-monospace
// @namespace        https://snydev.com/greasemonkey/
// @description      Changes the posting textarea font to monospace
// @include          https://debianforum.de/forum/posting.php*
// ==/UserScript==

var elements = document.getElementsByTagName('textarea')

for (var i in elements) {
	if(elements[i].style) {      
		elements[i].style.fontFamily = 'monospace';
	}
}
