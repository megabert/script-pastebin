// ==UserScript==
// @name     psychologieforum_hide_user_threads
// @author   some_random_dude
// @version  1
// @grant    none
// @namespace some_random_dude
// @include /https://www.psychologieforum.de(/search.php|/.*/$)/
// ==/UserScript==
//
//  *** Purpose ***
//
//  Hide Threads for specific users in Search / Subforum - view
//
//  *** Installation ***
//
//  1 Install Browser Extension
//  1.1 If Browser firefox: Install "greasemonkey" extension
//  1.2 If Browser google chrome: Install "tampermonkey" extension
//  
//  2 Install this Script
//  2.1 Copy the complete text of this script in a new greasmonkey/tampermonkey-script
//
//  3 Specify users to block in variable hide_users below
//
//

(function() {

// Fill in the users to block in the following line
var hide_users = ["Some_Username_I_do_not_like", "Some_other_User_I_do_not_like"];

var debug = false;

function my_log(msg) {
  if(debug) {
     unsafeWindow.console.log(msg);
  }
}
// Hide user Function for subforums
function hide_elements(page_type) {

  let list = document.getElementsByClassName("username");
  let row ="";
  let pp ="";

  for (var i = 0; i < list.length; i++) {
      for(var j = 0; j < hide_users.length; j++) {
      my_log(list[i].textContent + ":" + hide_users[j]);
      if (list[i].textContent == hide_users[j]) {
        pp=list[i].parentElement.parentElement;
        my_log("pp type: >" + pp.classList + "< author: >" + hide_users[j]+"<");
        if( (page_type==="subforum" && pp.classList == "author") || page_type === "search" ) {
          row=list[i].parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
          my_log("found thread of ignoried user: " +hide_users[j]);
        }
        my_log("row : "+row);
        if(row) {
          my_log("hiding row for ignored user");
        	row.style.display = "none";
        }
        break;
      }
    }
  }

}

function hide_user_content(hide_users) {

  var list = document.getElementsByClassName("searchlisthead");
  var page_type="";
  if(list.length == 0) {
    page_type="subforum";
  } else {
    page_type="search";
  }
  my_log("Page Type: "+page_type);
  hide_elements(page_type);
}

hide_user_content(hide_users)
})();