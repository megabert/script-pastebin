# Blocklist data updates for Postfix  
  
## General  
  
With this script you can regularly retrieve and create ip blocklists from blocklist.de and abuseipdb.com to reject mail server clients based on that. You also can use those lists to block clients requesting other services e. g. ssh, tcpd, ...  
  
For usage check out the shellscript.  
  
## Details on the operation  
  
This uses the two blocklists, merges them and creates a postfix access(5) file for postfix to check and reject the clients. You need the following snippet in your postfix main.cf to use the list:  
  
```
smtpd_client_restrictions = 
     check_client_access hash:/etc/postfix/ip_blocklists, ...
```
Postfix needs to do a reload, when the data is updated - which is done by the script if activated.
