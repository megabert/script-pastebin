#!/usr/bin/env php
<?php 

#
#	fridge-power-logger.php
#
#	check power usage of tasmota device every 10 seconds and write it into an sqlite database
#
#	- use tasmota publish/subscribe to retrieve data vi MQTT-Broker
#	- get Data in JSON-format from MQTT-Broker
#
#	- really bad code-hackfleisch in PHP
#
#	CAUTION: 
#
#	- this program should be the only device retrieving data for the mqtt-target device or else the 
#	  data retrieval will not be possible (additional messages are not handled here and will disturb 
#	  this program)
#

# --- program defaults 
#
$user="default_user";
$user="some_password";
$host="localhost";
$tasmota_device="tasmota_ABCDEF";
$sqlite_db="fridge_power_usage.sqlite";

# --- user settings here
#
require "settings.php";

$tasmota_dev_sub		= sprintf("stat/%s/#" ,$tasmota_device);	# subscription topic for the used device
$tasmota_dev_cmd_template	= sprintf("cmnd/%s/%%s",$tasmota_device); 	# command template for the used device
$db_values_max_age_seconds	= 60 * 60 * 24 * 3; # drop data older than 3 days

function my_sqlite_open($db_path) {

	global $written_records_since_db_open;
	$written_records_since_db_open=0;
	$dbh = new SQLite3($db_path);
	if ($dbh) {
		return $dbh;
	} else {
		dbg("could not open sqlite db: $db_path");
		return NULL;
	}
}

function mqtt_open_subscription($user,$host,$pass,$topic) {

	$descriptorspec = array(
	   0 => array("pipe", "r"), 
	   1 => array("pipe", "w"), 
	   2 => array("pipe", "w"));

	$process = proc_open("/usr/bin/mosquitto_sub -h '$host' -u '$user' -P '$pass' -t '$topic'", $descriptorspec, $pipes);
	fclose($pipes[0]);
	fclose($pipes[2]);

	stream_set_timeout($pipes[1], 60);

	return [ $process, $pipes[1] ];
				
}

function mqtt_send($topic,$data) {
	global $user, $host, $pass;
	# print ("topic: $topic data: $data\n");
	shell_exec("/usr/bin/mosquitto_pub -u $user -P '$pass' -h '$host' -t '$topic' -m '$data'");
}

function mqtt_command($command,$data) {
	global $tasmota_dev_cmd_template;
	mqtt_send(sprintf($tasmota_dev_cmd_template,$command),$data);
}

function mqtt_read($stream) {

	# read exactly one message from the handle
	$line=guarded_fgets($stream);
	return $line;
}


function guarded_fgets($read_stream)
   {

      # have a timeout on read_stream. Otherwise program will hang if no data could be read
	   
      $line = "";
      $time = 2;
      $c = "c";
      $read=array($read_stream);
      $w=[];
      $e=[];
      while ($time >= 0 && ord($c) != 10)
      {
         while (!feof($read_stream) &&
            ($n = stream_select($read, $w, $e, $time)) !== FALSE &&
            $n > 0 &&
            strlen($c = fgetc($read[0])) > 0 &&
            ord($c) != 10)
               $line .= $c;
         --$time;
      }

      if (ord($c) != 10)
      {
         // error handling
      }

      return $line;
   }

function get_power_from_json($obj) {
	$power=null;
	if(
			is_array($obj) 
		and	array_key_exists("StatusSNS",$obj)
		and	array_key_exists("ENERGY",$obj["StatusSNS"])
		and     array_key_exists("Power",$obj["StatusSNS"]["ENERGY"]) )
	{
		$power 			= $obj["StatusSNS"]["ENERGY"]["Power"];
		$usage_yesterday	= $obj["StatusSNS"]["ENERGY"]["Yesterday"];
	}
	return [$power,$usage_yesterday];

}

function db_write($dbh,$time,$power) {
	global $written_records_since_db_open;
	$written_records_since_db_open++;
	dbg("011 INSERT INTO usagelog (timestamp, power) VALUES('$time','$power');"); 
	$dbh->query("INSERT INTO usagelog (timestamp, power) VALUES('$time','$power');"); 
}

function db_write_usage_last_day($dbh,$time,$kwh) {

	dbg("012 SELECT COUNT(1) AS mycount FROM dailyusage WHERE timestamp='$time';");
	$results = $dbh->query("SELECT COUNT(1) AS mycount FROM dailyusage WHERE timestamp='$time';");
	$row = $results->fetchArray();

	dbg("012b mycount=".$row["mycount"]);
	if ($row["mycount"] > 0) {
		#dailyusage for yesterday already in database
		dbg("012c daily usage for yesterday already in database");
		return;
	}

	dbg("013 INSERT INTO dailyusage(timestamp,kwh_last_day) VALUES('$time','$kwh');"); 
	$dbh->query("INSERT INTO dailyusage(timestamp,kwh_last_day) VALUES('$time','$kwh');"); 
}

function db_cleanup($dbh,$age) {

	# print("DELETE FROM usagelog where (timestamp+$age) < cast(strftime('%s') as float)\n");
	$dbh->query("DELETE FROM usagelog where (timestamp+$age) < cast(strftime('%s') as float)");

}

function dbg($msg) {
	print($msg."\n");
}

function main() {

	global $tasmota_subscribe_stream, $sqlite_db, $sqlite_dbh, $db_values_max_age_seconds, $user, $host, $pass, $tasmota_dev_sub;
	global $written_records_since_db_open;

	list($subscribe_process,$tasmota_subscribe_stream) = mqtt_open_subscription($user,$host,$pass,$tasmota_dev_sub);
	$sqlite_dbh = my_sqlite_open($sqlite_db);

	while(true) {
		# delete old records every 24 hours
		for($hour=1;$hour<=24;$hour++) {
			# reopen stream/db every hour (due to php stream timeout)
			for($count=1;$count<=360;$count++) {
				dbg("001 send command");
				mqtt_command("Status","10");
				dbg("002 read command");
				$line = mqtt_read($tasmota_subscribe_stream);
				#print("mqtt-read: $line\n");
				$obj  = json_decode($line,true);
				list($power,$usage_yesterday) = get_power_from_json($obj);
				dbg("002a power=$power usage_yesterday=$usage_yesterday");
				if($count==1) {
					$yesterday=new DateTime(date("Y-m-",time()).(date("d",time())-1));
					db_write_usage_last_day($sqlite_dbh,$yesterday->format("U"),$usage_yesterday);
				}
				# print("power: $power\n");
				if(isset($power)) { 
					dbg("003 dbwrite command");
					db_write($sqlite_dbh,time(),$power);
				}
				sleep(10);
			}
			dbg("010 wrote $written_records_since_db_open records since db open. Reopening sqlite-db and mqtt-subscription streams.");
			# print_r(["sqlite_dbh",$sqlite_dbh]);
			dbg("004 db close command");
			$sqlite_dbh->close();
			# print_r(["sqlite_dbh",$sqlite_dbh]);
			dbg("005a proc_get_status mosquitto_sub");
			$proc = proc_get_status($subscribe_process);
			dbg("005b kill mosquitto_sub");
			posix_kill($proc['pid'], SIGKILL);
			dbg("005c close mosquitto_sub");
			$res=pclose($tasmota_subscribe_stream);
			dbg("005d pclose_res: $res");

			dbg("006 open mosquitto_sub");
			list($subscribe_process,$tasmota_subscribe_stream) = mqtt_open_subscription($user,$host,$pass,$tasmota_dev_sub);
			dbg("007 open db");
			$sqlite_dbh			= my_sqlite_open($sqlite_db);
		}

		dbg("008 cleanup db start");
		db_cleanup($sqlite_dbh,$db_values_max_age_seconds);
		dbg("009 cleanup db end");
	}
}

main();

?>
