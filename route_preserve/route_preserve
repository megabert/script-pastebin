#!/bin/bash

#
#       route_preserve
#
#       preserve a route to the current default gw for specified hosts by setting host routes for it.
#       can remove the routes too.
#
#

get_defgw() {

	local i
	local defgw
	local maxwait=30
	for((i=1;i<maxwait;i++));do
		if [ -z "$defgw" ] ; then 
			defgw=$(/sbin/ip route show | awk '/^default/ {print $3;exit}')
			if [ -n "$defgw" ] ; then
				mylog "Default GW has been detected"
				echo "$defgw"
				return 0
			fi
		fi
		sleep 1
		mylog "still waiting $(( $maxwait - $i )) seconds for Default-GW to show up"
	done
	mylog "No Default GW could be determined"
	return 1
}


my_remove_route() {

	local ip="$1"
	local gw=$(/sbin/ip route show | awk -vip=$ip 'match($0,"^"ip" ") { print $3 }')

	#echo "removing route for $ip to $gw"

	if [ -n "$gw" ]; then
		/sbin/ip route delete $ip via $gw
	fi
}

my_preserve_route() {

	local defgw="$1"
	local ip="$2"
	# echo "preserving route for $ip to $defgw"
	
	# remove the route for this host prior to set it
	my_remove_route $ip

        /sbin/ip route add $ip/32 via $defgw
}

mylog() 	{ echo "$self: $*" >&2;		}
err_exit()	{ mylog "$1"; exit $2; 		}

main() {

	local action="$1"
	local ip
	local defgw
	shift

	defgw=$(get_defgw) || err_exit "Aborting because no default gw could be detected" 1
	for ip in "$@" ;do
		case $action in
			preserve) my_preserve_route $defgw $ip ;;
			remove)   my_remove_route   $ip ;;
		esac
	done
}

# --- program starts here --- 

if [ $# -lt 2 ] || [ $1 != "preserve" ] && [ $1 != "remove" ]; then
        echo 
        echo "$(basename $0): preserve/remove default route for a specific ip"
        echo
        echo "Usage: $(basename $0) preserve/remove <target-ip>"
        echo
        echo "  preserve/remove:        set a host route or remove a host route for the specific ip"
        echo
        echo "  target-ip:              ip for which default route should be removed/preserved"
        echo
        exit 1
fi

export readonly self=$(basename $0)
main "$@"
