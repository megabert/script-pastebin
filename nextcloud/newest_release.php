<?PHP

#
#	This script figures out the newest available Nextcloud version from the current updater
#
#	requires:
#		- up2date checked out git repository https://github.com/nextcloud/updater_server/ in
#		  subdirectory updater_server in the same directory of this script
#
#	- script prints the current major version number + 1st number of minor version number 
#	  of the most current stable nc version number: e. g. 25.0
#
#	- if option --full is passed as only argument, script prints the full version number of 
#	  the the most current stable nc version number: e. g. 25.0.1
#

$c = require "updater_server/config/config.php";
$a = array_keys($c["stable"]);
usort($a, 'version_compare');
$newest=$a[sizeof($a)-1];
$newest_sub=array_keys($c["stable"][$newest]);
$newest_idx=$newest_sub[sizeof($newest_sub)-1];
$newest_version=$c["stable"][$newest][$newest_idx]["latest"];
if(count($argv) > 1 && ($argv[1]=="--full")) {
        print($newest_version);
} else {
        preg_match("/([0-9]+\.[0-9]+)/",$newest_version,$matches);
        print($matches[1]);
}

?>
